<%-- 
    Document   : cart
    Created on : Jan 30, 2023, 6:05:05 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cart</title>
     <!-- Favicons -->
    <link href="assets/images/favicon.png" rel="icon">
    <link href="assets/images/apple-touch-icon.png" rel="apple-touch-icon">
  
    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <!-- Vendor CSS Files -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="vendor/quill/quill.snow.css" rel="stylesheet">
    <link href="vendor/quill/quill.bubble.css" rel="stylesheet">
    <link href="vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="vendor/simple-datatables/style.css" rel="stylesheet">
  
    <!-- Template Main CSS File -->
    <link href="assets/css/cart.css" rel="stylesheet">

    <!--LINK OF CART-->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&display=swap" rel="stylesheet">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!--LINK OF ACTION IN CART-->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    
</head>
<body>
    <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top d-flex align-items-center">
    <h1>help me</h1>

    <!--Logo-->
    <div class="d-flex align-items-center justify-content-between">
        <a href="index.html" class="logo d-flex align-items-center">
            <img src="assets/images/cart/pngwing.com.png" alt="">
            <span class="d-none d-lg-block">Bakasa</span>
          </a>
    </div><!-- End Logo -->

    <!--search bar-->
    <div class="search-bar">
      <form class="search-form d-flex align-items-center" method="POST" action="#">
        <input type="text" name="query" placeholder="Search" title="Enter search keyword">
        <button type="submit" title="Search"><i class="bi bi-search"></i></button>
      </form>
    </div><!-- End Search Bar -->

    <!--icons navigation-->
    <nav class="header-nav ms-auto">
      <ul class="d-flex align-items-center">

        <!--search icon-->
        <li class="nav-item d-block d-lg-none">
          <a class="nav-link nav-icon search-bar-toggle " href="#">
            <i class="bi bi-search"></i>
          </a>
        </li><!-- End Search Icon-->

        <li class="nav-item dropdown">

          <a class="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
            <i class="bi bi-bell"></i>
            <span class="badge bg-primary badge-number">4</span>
          </a><!-- End Notification Icon -->

          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications">
            <li class="dropdown-header">
              You have 4 new notifications
              <a href="#"><span class="badge rounded-pill bg-primary p-2 ms-2">View all</span></a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li class="notification-item">
              <i class="bi bi-exclamation-circle text-warning"></i>
              <div>
                <h4>Lorem Ipsum</h4>
                <p>Quae dolorem earum veritatis oditseno</p>
                <p>30 min. ago</p>
              </div>
            </li>

            <li>
              <hr class="dropdown-divider">
            </li>

            <li class="notification-item">
              <i class="bi bi-x-circle text-danger"></i>
              <div>
                <h4>Atque rerum nesciunt</h4>
                <p>Quae dolorem earum veritatis oditseno</p>
                <p>1 hr. ago</p>
              </div>
            </li>

            <li>
              <hr class="dropdown-divider">
            </li>

            <li class="notification-item">
              <i class="bi bi-check-circle text-success"></i>
              <div>
                <h4>Sit rerum fuga</h4>
                <p>Quae dolorem earum veritatis oditseno</p>
                <p>2 hrs. ago</p>
              </div>
            </li>

            <li>
              <hr class="dropdown-divider">
            </li>

            <li class="notification-item">
              <i class="bi bi-info-circle text-primary"></i>
              <div>
                <h4>Dicta reprehenderit</h4>
                <p>Quae dolorem earum veritatis oditseno</p>
                <p>4 hrs. ago</p>
              </div>
            </li>

            <li>
              <hr class="dropdown-divider">
            </li>
            <li class="dropdown-footer">
              <a href="#">Show all notifications</a>
            </li>

          </ul><!-- End Notification Dropdown Items -->

        </li><!-- End Notification Nav -->

        <li class="nav-item dropdown">

          <a class="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
            <i class="bi bi-cart"></i>
            <span class="badge bg-success badge-number">3</span>
          </a><!-- End Messages Icon -->


        </li><!-- End Messages Nav -->

        <li class="nav-item dropdown pe-3">

          <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
            <img src="assets/images/cart/profile-img.jpg" alt="Profile" class="rounded-circle">
            <span class="d-none d-md-block dropdown-toggle ps-2">K. Anderson</span>
          </a><!-- End Profile Iamge Icon -->

          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
            <li class="dropdown-header">
              <h6>Kevin Anderson</h6>
              <span>Web Designer</span>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                <i class="bi bi-person"></i>
                <span>My Profile</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                <i class="bi bi-gear"></i>
                <span>Account Settings</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="pages-faq.html">
                <i class="bi bi-question-circle"></i>
                <span>Need Help?</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="#">
                <i class="bi bi-box-arrow-right"></i>
                <span>Sign Out</span>
              </a>
            </li>

          </ul><!-- End Profile Dropdown Items -->
        </li><!-- End Profile Nav -->

      </ul>
    </nav><!-- End Icons Navigation -->

  </header><!-- End Header -->

    <div class="content">
        <div class="cart-wrap">
            <!-- <div clss="container"> -->
                <div class="row">
                    <div class="col-lg-8 cart">
                        <!--title-->
                        <div class="main-heading">Shopping Cart</div>
                        <!--table cart-->
                        <div class="table-cart">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Unit Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th >Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                        <div class="product">
                                            <div class="img-product">
                                                <img src="https://www.91-img.com/pictures/laptops/asus/asus-x552cl-sx019d-core-i3-3rd-gen-4-gb-500-gb-dos-1-gb-61721-large-1.jpg" alt="" class="mCS_img_loaded">
                                            </div>
                                            <div class="name-product">
                                                Apple iPad Mini
                                                <br>G2356
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="unit-price">
                                            $1,250.00
                                        </div>
                                        
                                    </td>
                                    <td class="product-count">
                                        <div class="quantity">
                                            <form action="#" class="count-inlineflex">
                                                <div class="qtyminus">-</div>
                                                <input type="text" name="quantity" value="1" class="qty">
                                                <div class="qtyplus">+</div>
                                            </form>
                                        </div>
                                       
                                    </td>
                                    <td>
                                          <div class="total">
                                              $6,250.00
                                          </div>
                                    </td>
                                    <td class="text-right"> 
                                        <div class="cart-like">
                                            <a title="" href="" class="btn btn-outline-success" data-toggle="tooltip" data-original-title="Save to Wishlist"> <i class="fa fa-heart"></i></a> 
                                        </div>
                                        
                                    </td>
                                    <td>
                                        <div class="cart-remove">
                                            <a href="" class="btn btn-outline-danger"> � Remove</a>
                                        </div>
                                    </td>
                                </tr>
                                    <tr>
                                      <td>
                                          <div class="product">
                                              <div class="img-product">
                                                  <img src="https://www.91-img.com/pictures/laptops/asus/asus-x552cl-sx019d-core-i3-3rd-gen-4-gb-500-gb-dos-1-gb-61721-large-1.jpg" alt="" class="mCS_img_loaded">
                                              </div>
                                              <div class="name-product">
                                                  Apple iPad Mini
                                                  <br>G2356
                                              </div>
                                          </div>
                                      </td>
                                      <td>
                                          <div class="unit-price">
                                              $1,250.00
                                          </div>
                                          
                                      </td>
                                      <td class="product-count">
                                          <div class="quantity">
                                              <form action="#" class="count-inlineflex">
                                                  <div class="qtyminus">-</div>
                                                  <input type="text" name="quantity" value="1" class="qty">
                                                  <div class="qtyplus">+</div>
                                              </form>
                                          </div>
                                         
                                      </td>
                                      <td>
                                          <div class="display-flex align-center">
                                              <div class="total">
                                                  $6,250.00
                                              </div>
                                          </div>
                                      </td>
                                      <td class="text-right"> 
                                          <div class="cart-like">
                                              <a title="" href="" class="btn btn-outline-success" data-toggle="tooltip" data-original-title="Save to Wishlist"> <i class="fa fa-heart"></i></a> 
                                          </div>
                                          
                                      </td>
                                      <td>
                                          <div class="cart-remove">
                                              <a href="" class="btn btn-outline-danger"> � Remove</a>
                                          </div>
                                      </td>
                                  </tr>
                                </tbody>
                            </table>
                            
                        </div>
                        <!--end table-cart -->
                    </div>
                    <!-- cart check out -->
                    <div class="col-lg-4 check-out" >
                        <div class="cart-summary">
                            <h3>Summary</h3>
                            <form action="#" method="get" accept-charset="utf-8">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Subtotal</td>
                                            <td class="subtotal">$2,589.00</td>
                                        </tr>
                                        <tr>
                                            <td>Shipping</td>
                                            <td class="free-shipping">Free Shipping</td>
                                        </tr>
                                        <tr class="coupon-row">
                                         <!--coupon-box-->
                                            <div class="coupon-box">
                                              <form action="#" method="get" accept-charset="utf-8">
                                                  <div class="coupon-input">
                                                    <td class="coupon-text">
                                                      <input type="text" name="coupon code" placeholder="Coupon Code">
                                                    </td>
                                                    <td class="coupon-submit">
                                                      <button type="submit" class="round-black-btn apply-coupon">Apply Coupon</button>
                                                    </td>
                                                  </div>
                                              </form>
                                            </div>
                                        </tr>
                                        <tr class="total-row">
                                            <td>Total</td>
                                            <td class="price-total">$1,591.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="btn-cart-totals">
                                    <a href="#" class="update round-black-btn" title="">Update Cart</a>
                                    <a href="#" class="checkout round-black-btn" title="">Proceed to Checkout</a>
                                </div>
                                <!-- /.btn-cart-totals -->
                            </form>
                            <!-- /form -->
                        </div>
                        <!-- /.cart-totals -->
                    </div>
                    <!-- end cart check out -->
                </div>
            <!-- </div> -->
        </div>
    </div>

    <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright <strong><span>NiceAdmin</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
      Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

</body>

<!-- Vendor JS Files -->
<!--JS IN NAVBAR(FOR NOTIFICATION AND MESSAGES)-->
<script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/chart.js/chart.umd.js"></script>
<script src="assets/vendor/echarts/echarts.min.js"></script>
<script src="assets/vendor/quill/quill.min.js"></script>
<script src="assets/vendor/simple-datatables/simple-datatables.js"></script>
<script src="assets/vendor/tinymce/tinymce.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>
</html>